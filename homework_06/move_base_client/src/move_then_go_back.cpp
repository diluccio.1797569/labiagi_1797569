#include <csignal>
#include "ros/ros.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "move_base_msgs/MoveBaseAction.h"
#include "actionlib/client/simple_action_client.h"

double initial_pos_x, initial_pos_y, initial_or_z, initial_or_w;
bool wait;

void sigint_handler(int sig_num)
{
	wait = false;
}

void initial_pose_callback(const geometry_msgs::PoseWithCovarianceStamped &msg)
{
	ROS_INFO("Setting initial position...");
	initial_pos_x = msg.pose.pose.position.x;
	initial_pos_y = msg.pose.pose.position.y;
	initial_or_z = msg.pose.pose.orientation.z;
	initial_or_w = msg.pose.pose.orientation.w; 
	ROS_INFO("Initial position set with success! :)");
	ROS_INFO("POSITION x: %lf, y: %lf, z: 0.0", initial_pos_x, initial_pos_y);
	ROS_INFO("ORIENTATION x: 0.0, y: 0.0, z: %lf, w: %lf", initial_or_z, initial_or_w);
	wait = false;
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "move_then_go_back_node", ros::init_options::NoSigintHandler);
	
	ros::NodeHandle nh{};
	
	ROS_INFO("Setting SIGINT handler...");
	if (std::signal(SIGINT, sigint_handler) == SIG_ERR)
	{
		ROS_ERROR("Failed to set SIGINT handler! :(");
		ROS_ERROR("Aborting...");
		return -1;
	}
	ROS_INFO("SIGINT handler set with success! :)");
	
	ros::Subscriber sub_initial_pose = nh.subscribe("/initialpose", 1, initial_pose_callback);
	
	ros::Rate waiting_rate{10};
	
	wait = true;
	ROS_INFO("Waiting for the initial position to be received...");
	while (wait)
	{
		ros::spinOnce();
		waiting_rate.sleep();
	}
	ROS_INFO("Initial position received with success! :)");
	
	actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> move_base_client{"/move_base", true};
	
	ROS_INFO("Waiting for the move_base server to come up...");
	while (! move_base_client.waitForServer())
	{
		ros::spinOnce();
		waiting_rate.sleep();
	}
	ROS_INFO("Move_base server came up with success! :)");
	wait = true;
	
	ros::Duration waiting_duration{10};
	move_base_msgs::MoveBaseGoal move_base_goal{};
	move_base_msgs::MoveBaseGoal move_base_go_back{};
	
	move_base_goal.target_pose.header.stamp = ros::Time::now();
	move_base_goal.target_pose.header.frame_id = "odom" /*"map"*/;
	move_base_goal.target_pose.pose.position.x = -19.904;
	move_base_goal.target_pose.pose.position.y = 20.852;
	move_base_goal.target_pose.pose.position.z = 0.0;
	move_base_goal.target_pose.pose.orientation.x = 0.0;
	move_base_goal.target_pose.pose.orientation.y = 0.0;
	move_base_goal.target_pose.pose.orientation.z = 0.973;
	move_base_goal.target_pose.pose.orientation.w = -0.231;
	ROS_INFO("Sending the goal to the move_base server...");
	move_base_client.sendGoal(move_base_goal);
	ROS_INFO("Goal sent to the move_base server with success! :)");
	ROS_INFO("Waiting for a SIGINT to cancel the last goal...");
	while (wait)
	{
		ros::spinOnce();
		waiting_rate.sleep();
	}
	ROS_INFO("A SIGINT has just been caught! :)");
	wait = true;
	ROS_INFO("Cancelling the last goal...");
	move_base_client.cancelAllGoals();
	ROS_INFO("Last goal cancelled with success! :)");
	ROS_INFO("Setting SIGINT handler...");
	if (std::signal(SIGINT, sigint_handler) == SIG_ERR)
	{
		ROS_ERROR("Failed to set SIGINT handler! :(");
		ROS_ERROR("Aborting...");
		return -1;
	}
	ROS_INFO("SIGINT handler set with success! :)");
	ROS_INFO("Waiting for a SIGINT to send the goal to go back to the move_base server...");
	while (wait)
	{
		ros::spinOnce();
		waiting_rate.sleep();
	}
	ROS_INFO("A SIGINT has just been caught! :)");
	move_base_go_back.target_pose.header.stamp = ros::Time::now();
	move_base_go_back.target_pose.header.frame_id = "map";
	move_base_go_back.target_pose.pose.position.x = initial_pos_x;
	move_base_go_back.target_pose.pose.position.y = initial_pos_y;
	move_base_go_back.target_pose.pose.position.z = 0.0;
	move_base_go_back.target_pose.pose.orientation.x = 0.0;
	move_base_go_back.target_pose.pose.orientation.y = 0.0;
	move_base_go_back.target_pose.pose.orientation.z = initial_or_z;
	move_base_go_back.target_pose.pose.orientation.w = initial_or_w;
	ROS_INFO("Sending the goal to go back to the move_base server...");
	move_base_client.sendGoal(move_base_go_back);
	ROS_INFO("Goal to go back sent to the move_base server with success! :)");
	ROS_INFO("Waiting for the move_base server to complete the goal to go back...");
	std::signal(SIGINT, SIG_DFL);
	move_base_client.waitForResult();
	if (move_base_client.getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
		ROS_INFO("Move_base server completed the goal to go back with success! :)");
	else
		ROS_ERROR("Move_base server failed to complete the goal to go back! :(");
	return 0;
}
