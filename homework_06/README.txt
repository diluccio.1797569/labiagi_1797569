#############################################################################################
 
1) Move the 'move_base_client' folder in a catkin-ROS workspace in the package 'ros_tutorials/turtlesim'

2) Move into the workspace and compile it by running 'catkin build'
 
3) Open a terminal and run 'roscore'

4) Open a new terminal or a new tab of the same terminal, move into the 'stage_ros' package with 'roscd stage_ros' and run Stage with 'rosrun stage_ros stageros world/willow-erratic.world'

5) Open a new terminal or a new tab of the same terminal, move into the 'Documenti' directory with 'cd ~/Documenti' and run the ROS map server node with 'rosrun map_server map_server map.yaml'

6) Open a new terminal or a new tab of the same terminal, move into the 'labaigi_ws' workspace with '~/workspaces/labaigi_ws', source the workspace with 'source devel/setup.bash' and run the localizer node with 'rosrun thin_navigation thin_localizer_node'

7) Open a new terminal or a new tab of the same terminal, source the workspace with 'source devel/setup.bash' and run the planner node with 'rosrun thin_navigation thin_planner_node'

8) Open a new terminal or a new tab of the same terminal, move into the 'catkinROS_ws' workspace with '~/workspaces/catkinROS_ws', source the workspace with 'source devel/setup.bash' and run the homework with 'rosrun move_base_client move_then_go_back_node'

9) Open a new terminal or a new tab of the same terminal, run RViz with 'rosrun rviz rviz' and set the environment

10) Open a new terminal or a new tab of the same terminal, set the initial position for the localizer with 'rostopic echo /initialpose <TAB> <TAB>' and the fill in the field of the data structure

11) Check the results by watching the simulation on RViz and the message on the various terminals
 
#############################################################################################
