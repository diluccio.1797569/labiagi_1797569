#############################################################################################
 
1) Move the 'edge_extractor' folder in a catkin-ROS workspace

2) Move into the workspace and compile it by running 'catkin build'
 
3) Open a terminal and run 'roscore'

4) Open a new terminal or a new tab of the same terminal, move into '~/Documenti' and run the duckietow bag with 'rosbag play --pause duckietown_dataset.bag' then do a step in order to activate all the required ros topics.

5) Open a new terminal or a new tab of the same terminal and run the 'republish' node from 'image_transport' package which transform the compressed image from the '/default/camera_node/image/compressed' topic in a raw image in the '/default/camera_node/image/uncompressed' topic with 'rosrun image_transport republish compressed in:=default/camera_node/image _image_transport:=compressed raw out:=default/camera_node/image/uncompressed'

6) Open a new terminal or a new tab of the same terminal, move into the workspace, run 'source devel/setup.bash' and run the edge_extractor node with 'rosrun edge_extractor edge_extractor_node' then check the result by watching the two created windows 'Image' and 'Edges'
 
#############################################################################################
