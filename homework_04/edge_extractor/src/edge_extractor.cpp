#include <csignal>
#include "ros/ros.h"
#include "sensor_msgs/Image.h"
#include "sensor_msgs/image_encodings.h"
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "cv_bridge/cv_bridge.h"
#include "image_transport/image_transport.h"

void sigint_handler(int sig_num)
{
	ROS_INFO("Closing windows...");
	cv::destroyAllWindows();
	ROS_INFO("Windows closed with success! :)");
	ros::shutdown();
}

void image_callback(const sensor_msgs::ImageConstPtr &msg)
{
	cv::Mat img = cv_bridge::toCvCopy(msg, "bgr8")->image;
	cv::Mat grey_img = cv_bridge::toCvCopy(msg, "mono8")->image;
	
	GaussianBlur(grey_img, grey_img, cv::Size{3, 3}, 0, 0);
	Canny(grey_img, grey_img, 50.0, 50.0);
	imshow("Image", img);
	imshow("Edges", grey_img);
	cv::waitKey(100);
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "edge_extractor_node", ros::init_options::NoSigintHandler);
	
	ros::NodeHandle nh{};
	
	ROS_INFO("Installing custom SIGINT handler...");
	if (std::signal(SIGINT, sigint_handler) == SIG_ERR)
	{
		ROS_ERROR("Failed to install custom SIGINT handler! :(");
		ROS_INFO("Aborting...");
		return -1;
	}
	ROS_INFO("Custom SIGINT handler installed with success! :)");
	ROS_INFO("Opening windows...");
	cv::namedWindow("Image");
	cv::waitKey(100);
	cv::namedWindow("Edges");
	cv::waitKey(100);
	ROS_INFO("Windows opened with success! :)");
	
	image_transport::ImageTransport img_tr{nh};
	image_transport::Subscriber sub_image = img_tr.subscribe("/default/camera_node/image/uncompressed", 1000, image_callback);
	ros::spin();
	return 0;
}
