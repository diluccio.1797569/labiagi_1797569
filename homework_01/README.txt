#############################################################################################
 
1) Move the 'orazio_wrapper' folder in 'srrg2_orazio/srrg2_orazio' folder

2) Run 'make' in 'srrg2_orazio/srrg2_orazio/host_build'
 
3) Fill the methods in 'orazio_wrapper.cpp' marked with TODO 

4) Compile using 'make' in this folder and check the solution with 'orazio_wrapper_test'
 
#############################################################################################
