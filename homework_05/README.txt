#############################################################################################
 
1) Move the 'dits.py' file in a Python virtual environment with at least these libraries installed:
numpy, opencv, tensorflow, keras and the dataset.
 
2) Open a terminal and source the virtual environment

3) Run the script with 'python dits.py'

4) Check the results by reading the messages on the terminal

#############################################################################################
