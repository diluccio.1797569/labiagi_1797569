import numpy as np
import random as rd
import cv2
import keras.utils.np_utils
import keras.models
import keras.layers
import os

train_path = "./train"
test_path = "./test"
image_size = (32, 32)

def load_dits_data():
	data = list()
	x_train = list()
	y_train = list()
	x_val = list()
	y_val = list()
	x_test = list()
	y_test = list()
	num_classes = 0
	
	print("Reading train images...")
	for entry_dir in os.scandir(train_path):
		print("Processing directory " + entry_dir.path + "...")
		i = 0
		for file_dir in os.scandir(entry_dir.path):
			image = cv2.imread(file_dir.path, cv2.IMREAD_UNCHANGED)
			image = cv2.resize(image, image_size, cv2.INTER_LINEAR)
			label = int(entry_dir.name)
			data.append((image, label, i % 4 == 0))
			i += 1
		print("Directory " + entry_dir.path + " processed.")
		num_classes += 1
	print("Train images read.")
	print("Shuffling read train images...")
	rd.shuffle(data)
	print("Read train images shuffled...")
	print("Preparing train dataset...")
	for image, label, for_validation in data:
		if for_validation:
			x_val.append(image)
			y_val.append(label)
		else:
			x_train.append(image)
			y_train.append(label)
	x_train = np.array(x_train)
	y_train = np.array(y_train)
	y_train = keras.utils.np_utils.to_categorical(y_train, num_classes)
	x_val = np.array(x_val)
	y_val = np.array(y_val)
	y_val = keras.utils.np_utils.to_categorical(y_val, num_classes)
	print("Train dataset prepared.")
	data = list()
	print("Reading test images...")
	for entry_dir in os.scandir(test_path):
		print("Processing directory " + entry_dir.path + "...")
		i = 0
		for file_dir in os.scandir(entry_dir.path):
			image = cv2.imread(file_dir.path, cv2.IMREAD_UNCHANGED)
			image = cv2.resize(image, image_size, cv2.INTER_LINEAR)
			label = int(entry_dir.name)
			data.append((image, label))
			i += 1
		print("Directory " + entry_dir.path + " processed.")
	print("Test images read.")
	print("Shuffling read test images...")
	rd.shuffle(data)
	print("Read test images shuffled...")
	print("Preparing test dataset...")
	for image, label in data:
		x_test.append(image)
		y_test.append(label)
	x_test = np.array(x_test)
	y_test = np.array(y_test)
	y_test = keras.utils.np_utils.to_categorical(y_test, num_classes) 
	print("Test dataset prepared.")
	return x_train, y_train, x_val, y_val, x_test, y_test


print("Building dataset...")

x_train, y_train, x_val, y_val, x_test, y_test = load_dits_data()

print("Dataset built.")
print("Creating neural network model...")

model = keras.models.Sequential([
	keras.layers.Conv2D(32, (5, 5), activation = "relu",
		input_shape = (32, 32, 3), padding = "same", use_bias = True),
	keras.layers.MaxPooling2D(pool_size = (2, 2)),
	keras.layers.Dropout(0.2),

	keras.layers.Conv2D(32, (3, 3), activation = "relu",
		padding = "same", use_bias = True),
	keras.layers.MaxPooling2D(pool_size = (2, 2)),
	keras.layers.Dropout(0.2),
	keras.layers.BatchNormalization(),
	keras.layers.Dropout(0.2),
	
	keras.layers.Flatten(),
	keras.layers.Dropout(0.1),

	keras.layers.Dense(units = 512, activation = "relu",
		use_bias = True),
	keras.layers.Dropout(0.3),
	
	keras.layers.Dense(units = 59, activation = "softmax",
		use_bias = True)
])

model.summary()
print("Neural network model created.")
print("Compiling model...")
model.compile(optimizer = "adam", loss = "categorical_crossentropy",
	metrics = ["accuracy"])
print("Model compiled.")
print("Training model...")
model.fit(x_train, y_train, batch_size = 128, epochs = 10, verbose = 1,
	validation_data = (x_val, y_val))
print("Model trained.")
print("Saving model...")
model.save("./model/dits_model2.h5")
print("Model saved in './model/dits_model2.h5'.")
del model
print("Loading saved model...")
model = keras.models.load_model("./model/dits_model.h5")
print("Saved model loaded.")
print("Evaluating loaded model...")

score = model.evaluate(x_test, y_test, verbose = 1)
	
print(score)
print("Loaded model evaluated.")
print("Testing model...")

y_predicted = model.predict(x_test)

y_predicted = np.argmax(y_predicted, axis = 1)
y_test = np.argmax(y_test, axis = 1)
num_answers = y_predicted.shape[0]
num_correct_answers = 0

for i in range(num_answers):
	result = ""
	if (y_predicted[i] == y_test[i]):
		result = "RIGHT!"
		num_correct_answers += 1
	else:
		result = "WRONG!"
	print("Predicted: " + str(y_predicted[i]) + "\tCorrect: " +
		str(y_test[i]) + "\t-> " + result)
print("Final score: " + "\t" + str(num_correct_answers) + "/" +
	str(num_answers))
print(str(num_correct_answers / num_answers * 100.0) + "%")
