#############################################################################################
 
1) Move the 'tutorials' folder in a catkin-ROS workspace in the package 'ros_tutorials/turtlesim'

2) Move into the workspace and compile it by running 'catkin build'
 
3) Open a terminal and run 'roscore'

4) For all the nodes, open a new terminal or a new tab of the same terminal, run 'source
devel/setup.bash' and run
'rosrun turtlesim turtlesim_node'
'rosrun turtlesim turtle_teleop_key'
'rosrun turtlesim tutorial_draw_circle'
'rosrun turtlesim tutorial_delete_circle'
then check services '/spawn_circle', '/get_circles', '/remove_circles'
 
#############################################################################################
