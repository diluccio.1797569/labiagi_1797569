#include <cstdint>
#include <vector>
#include <string>
#include "ros/ros.h"
#include "turtlesim/Circle.h"
#include "turtlesim/Pose.h"
#include "turtlesim/Kill.h"
#include "turtlesim/GetCircles.h"
#include "turtlesim/RemoveCircle.h"

ros::Publisher old_circles_pub{};
ros::ServiceClient get_circles_cli{};
turtlesim::GetCircles get_circles_srv{};
ros::ServiceClient kill_cli{};
turtlesim::Kill kill_srv{};
std::vector<turtlesim::Circle> circle_msgs{};

bool remove_circle_with_iterator(const std::vector<turtlesim::Circle>::const_iterator &it)
{
	std::uint8_t id = it->id;
	
	ROS_INFO("Request: removing circle with id=%u...", id);
	ROS_INFO("Removing circle...");
	kill_srv.request.name = (std::string{"t"} + std::to_string(id)).c_str();
	
	bool result = kill_cli.call(kill_srv);
	
	if (result)
	{
		ROS_INFO("Circle removed with success! :)");
		circle_msgs.erase(it);
		
		turtlesim::Circle circle_msg{};
		
		circle_msg.id = id;
		old_circles_pub.publish(circle_msg);
	}
	else
		ROS_ERROR("Failed to remove circle! :(");
	return result;
}

bool remove_circle_callback(turtlesim::RemoveCircle::Request &req, turtlesim::RemoveCircle::Response &res)
{
	bool result;
	auto end = circle_msgs.cend();
	
	for (auto it = circle_msgs.cbegin(); it != end; ++it)
		if (it->id == req.id)
		{
			result = remove_circle_with_iterator(it);
			break;
		}
	return result;
}

void new_circles_callback(const turtlesim::Circle &msg)
{
	ROS_INFO("Spawned a new circle at (x=%f, y=%f) with id=%u", msg.x, msg.y, msg.id);
	get_circles_cli.call(get_circles_srv);
	circle_msgs = std::vector<turtlesim::Circle>{get_circles_srv.response.circles};
}

void pose_callback(const turtlesim::Pose &msg)
{
	auto end = circle_msgs.cend();
	
	for (auto it = circle_msgs.cbegin(); it != end; ++it)
		if (std::abs(it->x - msg.x) < 0.2 && std::abs(it->y - msg.y) < 0.2)
		{
			ROS_INFO("Turtle hit a circle! :o");
			remove_circle_with_iterator(it);
			break;
		}
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "tutorial_delete_circle");
	
	ros::NodeHandle nh{};
	ros::ServiceServer remove_circle_ser = nh.advertiseService("remove_circle", remove_circle_callback);
	ros::Subscriber new_circles_sub = nh.subscribe("new_circles", 1, new_circles_callback);
	ros::Subscriber pose_sub = nh.subscribe("turtle1/pose", 5, pose_callback);
	
	get_circles_cli = nh.serviceClient<turtlesim::GetCircles>("get_circles");
	kill_cli = nh.serviceClient<turtlesim::Kill>("kill");
	old_circles_pub = nh.advertise<turtlesim::Circle>("old_circles", 1);
	ros::spin();
	return 0;
}
