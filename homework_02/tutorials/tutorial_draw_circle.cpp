#include <cstdint>
#include <vector>
#include <string>
#include "ros/ros.h"
#include "turtlesim/Circle.h"
#include "turtlesim/Spawn.h"
#include "turtlesim/SpawnCircle.h"
#include "turtlesim/GetCircles.h"

ros::Publisher circle_pub{};
ros::ServiceClient spawn_cli{};
turtlesim::Spawn spawn_srv{};
std::uint8_t curr_id = 0;
std::vector<turtlesim::Circle> circle_msgs{};

bool spawn_circle_callback(turtlesim::SpawnCircle::Request &req, turtlesim::SpawnCircle::Response &res)
{
	ROS_INFO("Request: spawn a circle at (x=%f, y=%f)", req.x, req.y);
	ROS_INFO("Spawning circle...");
	++curr_id;
	spawn_srv.request.x = req.x;
	spawn_srv.request.y = req.y;
	spawn_srv.request.theta = 0.0;
	spawn_srv.request.name = (std::string{"t"} + std::to_string(curr_id)).c_str();
	
	bool result = spawn_cli.call(spawn_srv);
	
	if (result)
	{
		ROS_INFO("Circle spawned with success! :)");
		
		turtlesim::Circle circle_msg{};
		
		circle_msg.id = curr_id;
		circle_msg.x = req.x;
		circle_msg.y = req.y;
		circle_msgs.push_back(circle_msg);
		circle_pub.publish(circle_msg);
		res.circles = std::vector<turtlesim::Circle>{circle_msgs};
	}
	else
		ROS_ERROR("Failed to spawn circle! :(");
	return result;
}

bool get_circles_callback(turtlesim::GetCircles::Request &req, turtlesim::GetCircles::Response &res)
{
	ROS_INFO("Request: get spawned circles");
	ROS_INFO("Getting %u spawned circles...", curr_id);
	res.circles = std::vector<turtlesim::Circle>{circle_msgs};
	ROS_INFO("Spawned circles got with success! :)");
	return true;
}

void old_circles_callback(const turtlesim::Circle &msg)
{
	ROS_INFO("Removed the circle with id=%u", msg.id);
	
	auto end = circle_msgs.cend();
	
	for (auto it = circle_msgs.cbegin(); it != end; ++it)
		if (it->id == msg.id)
		{
			circle_msgs.erase(it);
			break;
		}
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "tutorial_draw_circle");
	
	ros::NodeHandle nh{};
	ros::ServiceServer spawn_circle_ser = nh.advertiseService("spawn_circle", spawn_circle_callback);
	ros::ServiceServer get_circles_ser = nh.advertiseService("get_circles", get_circles_callback);
	ros::Subscriber old_circles_sub = nh.subscribe("old_circles", 1, old_circles_callback);
	
	circle_pub = nh.advertise<turtlesim::Circle>("new_circles", 1);
	spawn_cli = nh.serviceClient<turtlesim::Spawn>("spawn");
	ros::spin();
	return 0;
}
