#include <fstream>
#include <string>
#include <csignal>
#include "ros/ros.h"
#include "tf/transform_listener.h"
#include "sensor_msgs/LaserScan.h"

std::ofstream fout{};
tf::TransformListener *p_tf_listener;
tf::StampedTransform stf{};

void sigint_handler(int sig_num)
{
	ROS_INFO("Closing file...");
	fout.close();
	ROS_INFO("File closed with success! :)");
	ros::shutdown();
}

void scan_callback(const sensor_msgs::LaserScan &msg)
{
	ROS_INFO("New laser scan!");
	if (! p_tf_listener->canTransform(msg.header.frame_id, "/odom", ros::Time{0}))
		ROS_WARN("A transformation from /odom to %s doesn't exists! :o", msg.header.frame_id.c_str());
	else
		try
		{
			p_tf_listener->lookupTransform(msg.header.frame_id, "/odom", ros::Time{0}, stf);
			ROS_INFO("Laser %s %s", stf.child_frame_id_.c_str(), stf.frame_id_.c_str());
		}
		catch (tf::TransformException &ex)
		{
			ROS_ERROR("An error occured during looking up the transformation! :(");
			ROS_INFO("Can't write it in the file! :(");
		}
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "laser_mapper_to_file_node", ros::init_options::NoSigintHandler);
	
	ros::NodeHandle nh{};
	
	ROS_INFO("Installing custom SIGINT handler...");
	if (std::signal(SIGINT, sigint_handler) == SIG_ERR)
	{
		ROS_ERROR("Failed to install custom SIGINT handler! :(");
		ROS_INFO("Aborting...");
		return -1;
	}
	ROS_INFO("Custom SIGINT handler installed with success! :)");
	ROS_INFO("Opening file...");
	fout.open("/home/lorenzo/workspaces/catkin_ROS_ws/src/exercise_laser_mapper/src/laser_scan_msgs.txt");
	if (! fout.is_open())
	{
		ROS_ERROR("Failed to open the file! :(");
		ROS_INFO("Aborting...");
		return -2;
	}
	ROS_INFO("File opened with success! :)");
	ROS_INFO("Creating a transformation listener...");
	
	tf::TransformListener tf_listener{};
	
	p_tf_listener = &tf_listener;
	ROS_INFO("Transformation listener created with success! :)");
	
	ros::Subscriber scan_sub = nh.subscribe("scan", 5, scan_callback);
	
	ros::spin();
	ROS_INFO("Closing file...");
	fout.close();
	ROS_INFO("File closed with success! :)");
	return 0;
}
