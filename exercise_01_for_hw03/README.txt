#############################################################################################
 
1) Move the 'exercise_laser_mapper' folder in a catkin-ROS workspace

2) Edit the 'laser_scan_msgs.txt' file path in the 'laser_mapper_to_file.cpp' file

3) Move into the workspace and compile it by running 'catkin build'
 
4) Open a terminal and run 'roscore'

5) Open a new terminal or a new tab of the same terminal, run 'source devel/setup.bash' and run the node with 'rosrun exercise_laser_mapper laser_mapper_to_file_node' then check outputs on the terminal and on the 'laser_scan_msgs.txt'
 
#############################################################################################
